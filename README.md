# Spring Boot Kotlin With JETTY And Swagger

## Profiles

### Development (profile `dev` by default)
Run As `src\main\kotlin\com\example\demo\DemoApplication.kt`

Or

`mvn spring-boot:run`

### Release WAR (profile `release`)
`mvn -DskipTests=true -Prelease clean package`

## Swagger
- [Swagger UI](http://localhost:8080/swagger-ui.html)
- [Swagger JSON Specification](http://localhost:8080/swagger.json)