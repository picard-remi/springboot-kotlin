package com.example.demo.controller

import com.example.demo.model.Pojo
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import springfox.documentation.annotations.ApiIgnore

@RestController
class ApiController {

    @GetMapping("get")
    fun get(@RequestParam param: String): Pojo {
        return Pojo(param)
    }

    @GetMapping("get2")
    fun get2(@RequestParam param: String): Pojo {
        val p = Pojo(param);

        val champ2 = p.champ + "3"

        return Pojo(champ2)
    }
}