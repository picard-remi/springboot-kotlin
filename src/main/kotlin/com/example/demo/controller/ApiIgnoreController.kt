package com.example.demo.controller

import com.example.demo.model.Pojo
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import springfox.documentation.annotations.ApiIgnore

@RestController
@ApiIgnore
class ApiIgnoreController {

    @GetMapping("ignore")
    fun get(@RequestParam param: String): Pojo {
        return Pojo(param)
    }
}