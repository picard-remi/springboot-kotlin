package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiJavaController {
    @GetMapping("java")
    public String get(@RequestParam String param) {
        return param;
    }
}
